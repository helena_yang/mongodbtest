﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDBTest;
using System.Text.RegularExpressions;

string connectionString = "mongodb://127.0.0.1:27017";
string databaseName = "testDB";
string collectionName = "photo";

var client = new MongoClient(connectionString);
IMongoDatabase database = client.GetDatabase(databaseName);
var collection = database.GetCollection<PersonModel>(collectionName);

//新增人
var person = new PersonModel() { FirstName = "0", LastName = "0", FriendsInArray = new string[] { "2","23", "555" }, isRestricted = true, Number = 3};
await collection.InsertOneAsync(person);

FilterDefinitionBuilder<PersonModel> Filter = Builders<PersonModel>.Filter;
string[] st = new string[] { "0" };
string[] st2 = new string[] { "2" };
var filterFirst = Filter.In(data => data.FirstName, st);

//為非選項
var filterLast = Filter.Not(Filter.In(data => data.LastName, st2));

//錯誤:要符合欄位dataType
//var filter2 = Filter.Eq("isRestricted", "2");

//用BsonArray
var filterSpecialFriend = Filter.Eq("isRestricted", true) & Filter.Not(Filter.Eq("Friends", "2"));

//AnyIn，用string array
var anyIn = Filter.AnyIn("FriendsInArray", "2");
var anyIn2 = Filter.AnyIn(x => x.FriendsInArray, new List<string> { "2" });
// 為何不能用lambda寫呢?
//var anyIn3 = Filter.AnyIn(x => x.FriendsInArray, "2");



//一樣的結果，只是用不同方式定義FieldDefinition
var inExpressionFieldDefinition = new ExpressionFieldDefinition<PersonModel, bool>(x => x.isRestricted);
var inStringFieldDefinition = new StringFieldDefinition <PersonModel, bool>("isRestricted");
var filter = Filter.Eq(inExpressionFieldDefinition, true);
var filter2 = Filter.Eq(inStringFieldDefinition, true);
var filter3 = Filter.Eq("isRestricted", true);
var filter4 = Filter.Eq(x=>x.isRestricted, true);
//用linq也可以(乾其實是Mongo的指令:)
var re = Filter.Where(data => data.LastName.Equals("2"));

//全部
var f = Filter.Empty;

//Regex
var f1 = Filter.Regex(x=>x.LastName, new BsonRegularExpression("2"));
var f2 = Filter.Regex("LastName", new BsonRegularExpression("2"));

//in
string[] t = new string[] { "2", "1" };
var in1 = Filter.In(x=> x.LastName, new LinkedList<string>(t));

//AnyEq
//TODO : 再理解一下 TItem value
var anyEq = Filter.AnyEq("FirstName", "2");

//ElemMatch
//TODO : 再理解一下
var filterSpecialFriend2 = Filter.ElemMatch("FriendsInArray", Filter.Eq("FriendsInArray", "2"));

//Gte
var gte = Filter.Gte(x => x.Number, 1);
var gt = Filter.Gt("Number", 1);

//Lte
var lt = Filter.Lt("Number",  1);
var lte = Filter.Lte(x=>x.Number, 1);

//Or
var or = Filter.Or(anyIn, anyIn2);

//And
IEnumerable<FilterDefinition<PersonModel>> temp = new List<FilterDefinition<PersonModel>>() { anyIn, anyIn2 };
var result1 = await collection.FindAsync(Filter.And(temp));
var result2 = await collection.FindAsync(Filter.And(anyIn, anyIn2));

var result = await collection.FindAsync(or);

foreach(var r in result.ToEnumerable())
{
    Console.WriteLine(r.Id);
}

