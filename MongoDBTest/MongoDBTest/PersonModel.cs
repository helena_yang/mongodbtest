﻿

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBTest;

internal class PersonModel
{
    /// <summary>
    /// BsonId(設定為unique identifier，類似key但很不一樣，裡面的序列化、獨特性比SQL複雜很多，所以用string也合理)
    /// [BsonRepresentation(BsonType.ObjectId)]設定id為string or other
    /// </summary>
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public BsonArray Friends { get; set; }

    public string[] FriendsInArray { get; set; }

    public bool isRestricted { get; set; }   

    public int Number { get; set; } 
}
